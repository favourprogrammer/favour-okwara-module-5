import 'package:flutter/material.dart';
import 'package:todo_app/api/firebase_api.dart';
import 'package:todo_app/view/addtasks.dart';
import 'package:todo_app/view/editpage.dart';

class TodoBoard extends StatefulWidget {
  const TodoBoard({Key? key}) : super(key: key);

  @override
  State<TodoBoard> createState() => _TodoBoardState();
}

class _TodoBoardState extends State<TodoBoard> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => AddTasks(),
            ),
          );
        },
        child: Icon(Icons.add),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.fromLTRB(20, 64, 20, 24),
            child: Text(
              "Tasks",
              style: TextStyle(
                color: Colors.grey[800],
                fontSize: 24,
              ),
            ),
          ),
          Expanded(
            child: StreamBuilder(
              stream: FirebaseApi.readTodos(),
              builder: (_, AsyncSnapshot snapshot) {
                if (!snapshot.hasData) {
                  return Text("No data...");
                } else {
                  List todos = snapshot.data;

                  return ListView.builder(
                    itemCount: todos.length,
                    itemBuilder: (context, index) {
                      final todo = todos[index];

                      return Padding(
                        padding: EdgeInsets.symmetric(
                          vertical: 4,
                          horizontal: 24,
                        ),
                        child: InkWell(
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => EditPage(todo: todo),
                              ),
                            );
                          },
                          child: Container(
                            padding: EdgeInsets.all(8),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              color: todo.isComplete
                                  ? Color(0xFFEDEFF1)
                                  : Colors.white,
                            ),
                            child: Row(
                              children: [
                                Padding(
                                  padding: EdgeInsets.symmetric(
                                    horizontal: 8,
                                  ),
                                  child: InkWell(
                                    onTap: () {
                                      setState(() {
                                        todo.isComplete = !todo.isComplete;
                                        FirebaseApi.updateTodo(todo);
                                      });
                                    },
                                    child: Container(
                                      width: 26,
                                      height: 26,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(8),
                                        color: Colors.grey[200],
                                      ),
                                      child: todo.isComplete
                                          ? Icon(
                                              Icons.check,
                                              size: 20,
                                              color: Colors.blue,
                                            )
                                          : null,
                                    ),
                                  ),
                                ),
                                Text(
                                  "${todo.title}",
                                  style: todo.isComplete
                                      ? TextStyle(
                                          fontSize: 12,
                                          color: Color(0xFFB5B9BD),
                                          decoration:
                                              TextDecoration.lineThrough,
                                        )
                                      : TextStyle(
                                          fontSize: 12,
                                          color: Colors.grey[600],
                                        ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                  );
                }
              },
            ),
          ),
        ],
      ),
    );
  }
}
