import 'package:flutter/material.dart';
import 'package:todo_app/api/firebase_api.dart';
import 'package:todo_app/model/todo.dart';

class AddTasks extends StatefulWidget {
  AddTasks({Key? key}) : super(key: key);

  final Todo todo = new Todo(title: "");

  @override
  State<AddTasks> createState() => _AddTasksState();
}

class _AddTasksState extends State<AddTasks> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[50],
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 36),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      InkWell(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Icon(
                          Icons.close,
                          size: 16,
                          color: Colors.grey,
                        ),
                      ),
                    ],
                  ),
                ),
                TextFormField(
                  decoration: InputDecoration(
                    icon: Container(
                      width: 20,
                      height: 20,
                      decoration: BoxDecoration(
                        color: Colors.blue[200],
                        borderRadius: BorderRadius.circular(4),
                      ),
                    ),
                    hintText: 'Title',
                    border: InputBorder.none,
                  ),
                  initialValue: "${widget.todo.title}",
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                    color: Colors.grey[600],
                  ),
                  onChanged: (value) {
                    setState(() {
                      widget.todo.title = value;
                    });
                  },
                ),
                SizedBox(height: 36),
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(16),
                    color: Color(0xFFFAF6EA),
                  ),
                  child: Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: 16,
                      vertical: 8,
                    ),
                    child: TextFormField(
                      minLines: 5,
                      maxLines: 5,
                      keyboardType: TextInputType.multiline,
                      initialValue: "${widget.todo.description}",
                      style: TextStyle(color: Colors.amber),
                      decoration: const InputDecoration(
                        hintText: 'Write a note...',
                        hintStyle: TextStyle(
                          color: Colors.amber,
                        ),
                        border: InputBorder.none,
                      ),
                      onChanged: (value) {
                        setState(() {
                          widget.todo.description = value;
                        });
                      },
                    ),
                  ),
                ),
              ],
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 24),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  TextButton(
                    style: TextButton.styleFrom(
                      backgroundColor: Colors.blue[50],
                      primary: Colors.blue,
                    ),
                    onPressed: () {
                      if (widget.todo.title != "") {
                        FirebaseApi.createTodo(widget.todo);
                        Navigator.pop(context);
                      }
                    },
                    child: Row(
                      children: [
                        Icon(
                          Icons.check,
                          size: 20,
                        ),
                        SizedBox(width: 8),
                        Text("Save"),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
