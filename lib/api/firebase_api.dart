import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:todo_app/model/todo.dart';

class FirebaseApi {
  static Future<String> createTodo(Todo todo) async {
    // init connection to tasks and generate empty document
    final docTodo = FirebaseFirestore.instance.collection('todo').doc();

    todo.id = docTodo.id; // generate id
    await docTodo.set(todo.toJson());

    return docTodo.id;
  }

  static Stream<List<Todo>> readTodos() => FirebaseFirestore.instance
      .collection('todo')
      .snapshots()
      .map((snapshot) =>
          snapshot.docs.map((doc) => Todo.fromJson(doc.data())).toList());

  static void updateTodo(Todo todo) async {
    // Retrieve todo DocumentReference by id.
    final docTodo = FirebaseFirestore.instance.collection('todo').doc(todo.id);

    await docTodo.update(todo.toJson());
  }

  static void deleteTodo(Todo todo) async {
    final docTodo = FirebaseFirestore.instance.collection('todo').doc(todo.id);

    await docTodo.delete();
  }
}
